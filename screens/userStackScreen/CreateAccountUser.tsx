import * as React from "react";
import {RootStackScreenProps} from "../../types";
import {ActivityIndicator, Alert, Pressable, StyleSheet, TextInput} from "react-native";
import {useState} from "react";
import {Text, View} from "../../components/Themed";
import useFetchStore from "../../secureStorage/storageData";
import axios from "axios";

export default function CreateUser({navigation}:RootStackScreenProps<'Login'>){
    const [email, setEmail]=useState("");
    const [password, setPassword]=useState("");
    const [passConfirm,setPassConfirm]=useState("");
    const [loading,setLoading]=useState(false);
    const STORAGE=useFetchStore();

    function onChangeEmail(t:string):void{
        setEmail(t);
    }

    function onChangePassWord(t:string):void{
        setPassword(t);
    }

    function onConfirmPas(t:string):void{
        setPassConfirm(t);
    }

    function isMailValide(email:string):boolean{
        if(email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )){
            return true;
        }

        return false;

    }

    function isPasswordSecure(password:string):boolean{
        if(password.length<8){
            return false;
        }
        return true;
    }


    function onPress():void{
        if(!isMailValide(email)){
            Alert.alert("Erreur l'addresse email est incorrect ! ");
        }
        else if(!isPasswordSecure(password)){
            Alert.alert("Erreur le mot de passe est trop court ! ");
        }
        else if(password !== passConfirm){
            Alert.alert("Erreur les mots de passes ne correspondent pas ");
        }
        else{
            /**Define user then save it*/
            let user={
                userEmail:email,
                userPass:password
            }
            STORAGE.save("user",user);

            setLoading(true);
            setEmail("");
            setPassword("");
            setPassConfirm("");

            setTimeout(()=>{
                setLoading(false);
                navigation.navigate('Home');
            },2000)

        }
    }

    function onCancel():void{
        navigation.goBack();
    }

    if(loading===true)
        return loading && <ActivityIndicator />;

    return(
        <View style={styles.container}>
            <Text style={styles.titre}>Créer un compte </Text>

            <View style={styles.inputContainer}>
                <TextInput style={styles.input} value={email} onChangeText={onChangeEmail} placeholder="Email" ></TextInput>
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.input} value={password} secureTextEntry={true} onChangeText={onChangePassWord} placeholder="Mot de passe" ></TextInput>
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.input} value={passConfirm} secureTextEntry={true} onChangeText={onConfirmPas} placeholder="Confirmez Mot de passe" ></TextInput>
            </View>

            <View style={styles.buttonContainer}>
                <View style={styles.button}>
                    <Pressable onPress={()=>onPress()}>
                        <Text  style={styles.btn_text}>Créer mon compte</Text>
                    </Pressable>
                </View>
                <View style={styles.button}>
                    <Pressable onPress={onCancel}>
                        <Text  style={styles.btn_text}>annuler</Text>
                    </Pressable>
                </View>
            </View>

        </View>
    )
}

const styles= StyleSheet.create({
    container:{
        flexGrow:1,
        justifyContent: 'center',
        backgroundColor:"lightblue",
        paddingBottom:24
    },
    titre:{
        fontSize:24,
        fontWeight:"bold",
        textAlign:"center"
    },
    inputContainer:{
        marginHorizontal:24,
        marginBottom:12
    },
    input:{
        height:40,
        borderWidth:1,
        padding:10,
        borderRadius:2
    },
    buttonContainer:{
        marginHorizontal:24,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        backgroundColor:"lightblue",
    },
    button:{
        padding:12,
        backgroundColor:"black",
        borderWidth:1,
        borderRadius:6
    },
    btn_text:{
        color:"white",
        textAlign:"center",
    },
})
