
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {useEffect, useState} from "react";
import * as React from "react";
import {RootStackParamList, RootTabScreenProps} from "../../types";
import User from "../User";
import LoginScreen from "./LoginScreen";
import CreateUser from "./CreateAccountUser";
import useFetchStore from "../../secureStorage/storageData";

const Stack=createNativeStackNavigator<RootStackParamList>();
export default function UserNavigator({navigation}:RootTabScreenProps<'UserNavigator'>){
    const[state,setState]=useState(false)
    const [userToken, setUserToken]=useState<{token:string ,expiration?:number }| null>(null);
    const STORAGE=useFetchStore();
    useEffect(()=>{
        STORAGE.getValueFor("userToken").then((value:string |null)=>{
            console.log(value)
            if(value!=null){
                let json= JSON.parse(value)
                setUserToken({
                    token:json.token,
                    expiration:parseInt(json.expiration)
                });
            }
        });
        //STORAGE.deleteValue("userToken");
    },[])

    let currentDate = new Date().getTime();
    if(userToken!=null && userToken?.token!="" && userToken?.expiration>currentDate){
        let date =new Date(userToken.expiration);
        console.log("date ",date.getDate(),"heure",date.getHours()," minutes ",date.getMinutes(), "sec ",date.getSeconds())
        return (
            <Stack.Navigator>
                <Stack.Screen name="User" component={User} options={{ headerShown: false }} />
            </Stack.Navigator>
        );

    }
    else{
        return (
            <Stack.Navigator>
                <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
                <Stack.Screen name="CreateUser" component={CreateUser} options={{ headerShown: false }} />
                <Stack.Screen name="User" component={User} options={{ headerShown: false }} />
            </Stack.Navigator>
        );
    }



}

