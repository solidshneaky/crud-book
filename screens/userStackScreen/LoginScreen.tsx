import * as React from "react";
import {ActivityIndicator, Alert, Pressable, StyleSheet, TextInput} from "react-native";
import {RootStackScreenProps, RootTabScreenProps} from "../../types";
import {View, Text} from "../../components/Themed";
import {useState} from "react";
import axios from "axios";
import useFetchStore from "../../secureStorage/storageData";




export default function LoginScreen({navigation}:RootTabScreenProps<'UserNavigator'>){
    const [email, setEmail]=useState("");
    const [password, setPassword]=useState("");
    //const [user,setUser]=useState<{id:string ,email?:string,pass?:string} |null>(null);
    const [loading, setLoading]=useState(false);
    const STORAGE=useFetchStore();


    function onChangeEmail(t:string):void{
        setEmail(t);
    }

    function onChangePassWord(t:string):void{
        setPassword(t);
    }

    function isMailValide(email:string):boolean{
        if(email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )){
            return true;
        }
        return false;
    }
    function isPasswordSecure(password:string):boolean{
        if(password.length<8){
            return false;
        }
        return true;
    }

    const callBackEnd=async(username:string, pass:string)=>{
        const response= await axios({
            method: 'post',
            url: 'http://PUT YOUR IP HERE:3000/auth/login/',
            data: {
                username: username,
                password: pass
            }
        }).then(function (response) {
            //set token JWT and date expiration valide to 1 week
            let token ={
                token:response.data.access_token,
                expiration : new Date().getTime()+604800000
            }
            STORAGE.save("userToken",JSON.stringify(token));
            console.log("done")
        }).catch(error=>{
            console.log("error : ",error);
        });
    }
    function onPress():void{
        if(!isMailValide(email)){
            Alert.alert("Erreur l'addresse email est incorrect ! ");
        }
        /*else if(!isPasswordSecure(password)){
            Alert.alert("Erreur le mot de passe est trop court ! ");
        }*/
        else{
            setLoading(true);
            callBackEnd(email,password);

            setTimeout(()=>{
                    setLoading(false)
                    navigation.navigate("User");
            },2000)


            //part non necessary
            /*if(email===user?.email && password===user?.pass){
                setEmail("");
                setPassword("");
            }
            else{
                Alert.alert("Erreur merci de vérifier vos identifiants ! ");
            }*/
        }
    }
    function goToCreatAccount():void{
        navigation.navigate('CreateUser')
    }

    if(loading){
        return <ActivityIndicator />
    }
   return(
       <View style={styles.container}>
           <Text style={styles.titre}>LOGIN </Text>

           <View style={styles.inputContainer}>
                <TextInput style={styles.input} value={email} onChangeText={onChangeEmail} placeholder="Email" ></TextInput>
           </View>
           <View style={styles.inputContainer}>
                <TextInput style={styles.input} value={password} secureTextEntry={true} onChangeText={onChangePassWord} placeholder="Mot de passe" ></TextInput>
           </View>

           <View style={styles.buttonContainer}>
               <View style={styles.button}>
                   <Pressable onPress={onPress}>
                       <Text  style={styles.btn_text}>Valider</Text>
                   </Pressable>
               </View>
           </View>

           <View>

           </View>
           <Text>Mot de passe oublié ?</Text>
           <Pressable onPress={goToCreatAccount}>
               <Text>Crer un compte</Text>
           </Pressable>


       </View>
   )
}

const styles= StyleSheet.create({
    container:{
        flexGrow:1,
        justifyContent: 'center',
        backgroundColor:"lightblue",
        paddingBottom:24
    },
    titre:{
        fontSize:24,
        fontWeight:"bold",
        textAlign:"center"
    },
    inputContainer:{
        marginHorizontal:24,
        marginBottom:12
    },
    input:{
        height:40,
        borderWidth:1,
        padding:10,
        borderRadius:2
    },
    buttonContainer:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"lightblue",
    },
    button:{
        width:"50%",
        padding:12,
        backgroundColor:"black",
        borderWidth:1,
        borderRadius:6
    },
    btn_text:{
        color:"white",
        textAlign:"center",
    },
})
