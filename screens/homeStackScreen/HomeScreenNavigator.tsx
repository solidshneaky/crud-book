import * as React from "react";

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {RootStackParamList,} from "../../types";
import Home from "../Home";
import NoBookFoundScreen from "../noBookFoundScreen";
import {useState} from "react";

const Stack=createNativeStackNavigator<RootStackParamList>();
export default function HomeNavigator(){
    const [loading, setLoading] = useState(false);
    if(!loading){
        return (
            <Stack.Navigator>
                <Stack.Screen name="Start" component={NoBookFoundScreen} options={{ headerShown: false }} />
            </Stack.Navigator>
        );
    }
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}
