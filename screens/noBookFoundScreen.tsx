import * as React from "react";
import {Alert, FlatList, Pressable, StyleSheet} from "react-native";
import {Text, View} from "../components/Themed";
import {RootStackScreenProps} from "../types";
import CameraScreen from "../components/cameraScreen";
import {useState} from "react";

export default function NoBookFoundScreen({navigation}:RootStackScreenProps<'Home'>){
    type book={id:string, title:string} | undefined;
    const DATA:book[] = [
        {
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'First Item',
        },
        {
            id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
            title: 'Second Item',
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e29d72',
            title: 'Third Item',
        },
    ];

    const renderItem = ({item}:{item:book})=>{
        console.log(item)
        return(
                <Text>{item?.title}</Text>
            )
    }

    function onPress():void{
        // @ts-ignore
        navigation.navigate('Search');
    }

    const [showCamera, setShowCamera]=useState(false);
    function openCamera():void{
        setShowCamera(true);
    }

    function closeCamera():void{
        setShowCamera(!showCamera);
    }

    return(
        <View style={styles.container}>
            <Text style={styles.titre}>LIBRAIRIE</Text>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            <Text style={styles.para}>Vous n'avez pas encore lu de livres</Text>
            <Text style={styles.para}>Selectionnez votre première lecture</Text>
            <View style={styles.buttonContainer}>
                <View style={styles.button}>
                    <Pressable onPress={onPress}>
                        <Text  style={styles.btn_text}>COMMENCER</Text>
                    </Pressable>
                </View>
                <View style={styles.button}>
                    <Pressable onPress={openCamera}>
                        <Text style={styles.btn_text}>Open Camera</Text>
                    </Pressable>
                </View>
            </View>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            <FlatList
                data={DATA}
                keyExtractor={(item) => item.id}
                renderItem={renderItem} />


            {showCamera  &&
            <View style={styles.cameraContainer} >
                <View style={styles.btn_container}>
                    <Pressable onPress={closeCamera}>
                        <Text>Close camera</Text>
                    </Pressable>
                </View>
                <CameraScreen />
            </View>
            }
        </View>
    )
}
const styles=StyleSheet.create({
    container:{
        flexGrow:1,
        position:"relative"
    },
    titre:{
        paddingTop:50,
        fontSize:24,
        fontWeight:"bold",
        textAlign:"center"
    },
    separator: {
      margin:24,
        height: 1,
        width: '80%',
    },
    para:{
      textAlign:"center",
      margin:12
    },
    buttonContainer:{
     margin:24,
      flexDirection:"row",
      justifyContent:"center",
      alignItems:"center"
    },
    button:{
        width:"50%",
        padding:12,
        backgroundColor:"black",
        borderWidth:1,
        borderRadius:6
    },
    btn_text:{
        color:"white",
        textAlign:"center",
    },
    btn_container:{
    flexDirection:"row",
    justifyContent:"space-around",
    },
    closeCamere:{
        color:"white"
    },
    cameraContainer:{
        flex:1,
        position:"absolute",
        top:0,
        left:0,
        right:0,
        width:"100%",
        height:600,
        zIndex:100,

    },
})
