import { StyleSheet } from 'react-native';
import React, {useEffect, useState} from "react";
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import * as SecureStore from "expo-secure-store";
import { fakeData } from '../constants/fakeData';
import { FlatListBasics } from '../components/ListBook';

export default function Home({ navigation }: RootTabScreenProps<'HomeNavigator'>) {

    async function getValueFor(key:string): Promise<string | null>{
        let result=null;
        await SecureStore.getItemAsync(key).then(value => {
            setUser(value) ;
        });
        return result;
    }

    async function deleteValue(key:string):Promise<void>{
        await SecureStore.deleteItemAsync(key);
    }

    const [user,setUser]=useState<string|null>( null);

    useEffect(()=>{

        getValueFor("userID");

    },[]);

  return (
      <View style={styles.container}>
        <Text style={styles.title}>Derniers Livres Lus pour ID {user}</Text>
        <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)"/>
        <View style={styles.container}><FlatListBasics></FlatListBasics></View>
      </View>
  );
}

const styles=StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 40,
      fontWeight: 'bold',
    },
    separator: {
      marginVertical: 30,
      height: 1,
      width: '80%',
    },
})