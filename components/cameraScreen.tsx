//JUST A FUCKING TEST TO USE CAMERA
import { Camera } from 'expo-camera';
import {useEffect, useRef, useState,} from "react";
import {FlatList, Image, Pressable, ScrollView, StyleSheet, TouchableOpacity} from "react-native";
import {Text, View, } from "./Themed";
import * as React from "react";
import {CapturedPicture} from "expo-camera/build/Camera.types";
import useFetchStore from "../secureStorage/storageData";

export default function CameraScreen (){
    const [hasPermission, setHasPermission] = useState(null);
    const [displayFlatList, setDisplayFlatList]=useState<boolean>(false);
    const [data,setData]=useState<CapturedPicture[]>([])
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [photos, setPhoto]=useState<CapturedPicture[]>([]);
    const camera = useRef<Camera | null>(null);
    const STORAGE=useFetchStore();
    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasPermission(status === 'granted');
        })();

        //STORAGE.deleteValue("pictures")
    }, []);

    useEffect( () => {
        if(photos.length>0){
            console.log("picture saved")
            STORAGE.save("pictures",JSON.stringify(photos));
        }
        STORAGE.save("pictures",JSON.stringify(photos));
    },[photos])

    useEffect(() => {
            STORAGE.getValueFor("pictures").then(value => {
                setData(JSON.parse(value));
                setPhoto(JSON.parse(value))
            })
        console.log("gallerie : " , data);
    },[displayFlatList])

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    const renderItem = ({item}:{item:CapturedPicture})=>{

        return(
            <ScrollView>
                <View  style={{flexDirection:"row", justifyContent:"space-evenly"}}>
                        <Image
                            style={{width:100,height:100, margin:12}}
                            source={{uri:item.uri}}
                        />
                </View>
            </ScrollView>
        )
    }

    const takePicture = async ()=> {
        if (camera.current) {
            const photo = await camera.current.takePictureAsync()
                .then((value) => {
                    setPhoto([...photos,value]);
                    console.log("picture taking at ",new Date().getTime())
                })
                .catch(error=>{
                    console.log(error);
                })
        }
        else {
            console.log('asdfas')
        }
    }


   if(displayFlatList){
       return(

           <View style={styles.flatList}>
               <Pressable onPress={()=>setDisplayFlatList(false)}><Text>prendre une photo</Text></Pressable>
               <FlatList
                   data={data}
                   renderItem={renderItem}
                   keyExtractor={item=>item.uri}
               />
           </View>
       );
   }


    return (
            <Camera  type={type} style={styles.cameraContainer} ref={camera}>

                <View style={styles.cameraSubContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                setType(
                                    type === Camera.Constants.Type.back
                                        ? Camera.Constants.Type.front
                                        : Camera.Constants.Type.back
                                );
                            }}>
                            <Text style={styles.cameraButton}> flip </Text>
                        </TouchableOpacity>
                        <Pressable onPress={takePicture}>
                            <View style={styles.cameraSnap}></View>
                        </Pressable>
                        <Pressable onPress={()=>setDisplayFlatList(!displayFlatList)}>
                            <Text style={styles.cameraButton}>gallerie</Text>
                        </Pressable>
                </View>
            </Camera>
        );
}
const styles=StyleSheet.create({
    cameraContainer:{
        flex:1,
        marginTop:0,
        position:"relative",
        width:"100%",
        height:"100%",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"flex-end"
    },
    cameraSubContainer:{
        width:"100%",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:"transparent",
        marginBottom:50
    },
    cameraButton:{
        backgroundColor:"rgba(255,255,255,0.1)",
        fontSize:24,
        color:"white"
    },
    cameraSnap:{
        borderRadius:60,
        height:80,
        width:80,
        backgroundColor:"rgba(255,255,255,0.5)",
    },
    flatList:{
        position:"absolute",
        top:0,
        left:0,
        right:0,
        zIndex:999,
        width:"100%",
        height:"100%",

    }
})
