import React, {useState} from 'react';
import { Button, FlatList, StyleSheet } from 'react-native';
// import CheckBox from '@react-native-community/checkbox';
import Checkbox from 'expo-checkbox';
import { Text, View } from '../components/Themed';

export function FlatListBasics(){
    const datas=[
        {id:"1", title: 'Le rouge et le noir', author: "Stendhal", readed: true},
        {id:"2", title: 'Psychologie des foules', author: "Gustave Le bon", readed: false},
        {id:"3", title: 'Le chien des baskerville', author:"Doyle", readed:false},
      ];
    const [books, setNewBook] = useState(datas);
    function setCheck(item:any, e:any):void{
        let arr = [...books];
        if(arr[item].readed) arr[item].readed = false;
        else arr[item].readed = true;
        setNewBook(arr)
    }
    return (
        <FlatList
          data={books}
          renderItem={({item, index}) =><Text>{item.title} Autheur : {item.author}
          <Checkbox 
            style={styles.checkbox}
            value={item.readed}
            color={(item.readed) ? "green" : "grey"}
            onValueChange={(e) => setCheck(index, e)}
          ></Checkbox></Text>}
        />
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginHorizontal: 16,
      marginVertical: 32,
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    paragraph: {
      fontSize: 15,
    },
    checkbox: {
      margin: 8,
    },
  });